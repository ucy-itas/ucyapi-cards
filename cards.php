<?php
use Iss\Api\Handler;
use Iss\Api\Messaging\Response;
use Iss\Api\Messaging\Response\{Error, NotImplementedError, NotFoundError};

$application = Phalcon\Di::getDefault()->get('application');

$application->map('/cards', function () {
    switch (true) {
        case $this['request']->isOptions():
            return null;
        case $this['request']->isGet():
            $request = $this->Authorization->authorizeRequest($this['request']->getQuery());
            $result = (new Handler($this['config']('handler.card')))->getCollection($request);

            return new Response($result);
        default:
            return new NotImplementedError($this['uuid']);
    }
})->setName('cards-catalog');

$application->map('/cards/{card_number}', function ($card_number) {
    switch (true) {
        case $this['request']->isOptions():
            return null;
        case $this['request']->isHead():
        case $this['request']->isGet():
            if (is_card_number($card_number)) {
                $_GET['card_number'] = $card_number;
                $logger = $this['logger'];
                $logger['card_number'] = $card_number;
                $logger->info('Retrieving card [{card_number}] [{uuid}]');
                $request = $this->Authorization->authorizeRequest($this['request']->getQuery());
                $result = (new Handler($this['config']('handler.card')))->getResource($request);
                return new Response($result);
            } else {
                $result = new NotFoundError($this['uuid'], 'Card was not found');
            }
            return $result;
        default:
            return new NotImplementedError($this['uuid']);
    }
})->setName('card-by-number');

$application->map('/cards/{card_number}/photos', function ($card_number) {
    switch (true) {
        case $this['request']->isOptions():
            return null;
        case $this['request']->isGet():
            if (is_card_number($card_number)) {
                $_GET['card_number'] = $card_number;
                $logger = $this['logger'];
                $logger['card_number'] = $card_number;
                $logger->info('Retrieving photo for card number [{card_number}] [{uuid}]');
                $request = $this->Authorization->authorizeRequest($this['request']->getQuery());
                $result = (new Handler($this['config']('handler.photo')))->getResource($request);
                return new Response($result);
            } else {
                $result = new NotFoundError($this['uuid'], 'Card was not found');
            }
            return $result;
        default:
            return new NotImplementedError($this['uuid']);
    }
})->setName('card-photos-by-card-number');
